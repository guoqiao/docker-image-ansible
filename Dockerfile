FROM ubuntu:18.04

WORKDIR /root

COPY requirements.txt .

RUN apt-get update -y && \
    apt-get install -y python3-pip && \
    /usr/bin/pip3 install -U -r requirements.txt && \
    apt-get -y autoremove && apt-get -y autoclean && apt-get -y clean
